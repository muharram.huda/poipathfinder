
CREATE DATABASE cobahuda
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'en_US.UTF-8'
       LC_CTYPE = 'en_US.UTF-8'
       CONNECTION LIMIT = -1;


-- Table: tblpois

-- DROP TABLE tblpois;

CREATE TABLE tblpois
(
  id integer NOT NULL DEFAULT nextval('inc_poi'::regclass),
  lng numeric,
  lat numeric,
  CONSTRAINT id_primary PRIMARY KEY (id)
)
