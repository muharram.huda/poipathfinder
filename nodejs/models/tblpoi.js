/**
 * tblpoi model module.
 * @module model/tblpoi
 */

'use strict';

module.exports = function(sequelize, DataTypes) {
    /**
     * sequelize object from table tblpois
     * @function
     */
    var tblpoi = sequelize.define('tblpoi', {
        id: {type:DataTypes.INTEGER,autoIncrement: true,primaryKey: true},
        lat: DataTypes.DECIMAL,
        lng: DataTypes.DECIMAL
    },{
        timestamps: false,
        classMethods: {
            associate: function(models) {
            // associations can be defined here
            },
            /**
             * delete a POI coordinate
             * @function
             * @param {string} lat - latitude.
             * @param {string} lng - longitude.
             * @returns {function/promise} delete process promise
             */
            deletePOI: function(lat,lng) {
                return this.destroy( {
                    where: {
                        lat:lat,
                        lng:lng
                    }
                });
            },
            /**
             * add a POI coordinate
             * @function
             * @param {string} lat - latitude.
             * @param {string} lng - longitude.
             * @returns {function/promise} add process promise
             */
            addPOI: function(lat,lng) {
                return this.create( {
                    lat:lat,
                    lng:lng
                });
            },
            /**
             * get all POI coordinates
             * @function
             * @returns {function/promise} get all poi
             */
            getPOI: function() {
                return this.findAll();
            }

        }
    });
    return tblpoi;
};