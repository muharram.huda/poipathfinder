'use strict';
module.exports = function(sequelize, DataTypes) {
  var tes = sequelize.define('tes', {
    id: {type:DataTypes.INTEGER,primaryKey: true},
    name: DataTypes.TEXT
  },{
    timestamps: false
}, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return tes;
};