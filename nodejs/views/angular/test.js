var mainApp = angular.module("mainApp", ['ngMap']);

         mainApp.controller('studentController', function($scope,NgMap, $http) {
            $scope.student = {
               firstName: "Mahesh",
               lastName: "Parashar",

               fullName: function() {
                  var studentObject;
                  studentObject = $scope.student;
                  return studentObject.firstName + " " + studentObject.lastName;
               }

            };
            $scope.poiClicked = false;
            $scope.poiMarkClicked = false;
            $scope.poiMarkClick = "asdasdas"
            $scope.lat = 0;
            $scope.lng = 0;
            $scope.placeId = "";
            var map;
            var vm = this;
            $scope.googleMapsUrl="https://maps.googleapis.com/maps/api/js?key=AIzaSyBCwWBHFM9kBg3y5KTKuVRTJlTmIR_sGSc&libraries=places";
            NgMap.getMap().then(function(evtMap) {
             console.log(evtMap.getCenter());
             console.log('markers', evtMap.markers);
             console.log('shapes', evtMap.shapes);
             map = evtMap;
                vm.populateMarker()
            });
            $scope.dynMarkers = [];



            vm.markerClick = function(events, marker) {
                var pos = marker.$index;
//                console.log($scope.dynMarkers[pos]);
                $scope.lat = $scope.dynMarkers[pos].lat;
                $scope.lng = $scope.dynMarkers[pos].lng;
                $scope.poiMarkClicked = true;

            }

            vm.populateMarker = function()
            {
                $http.get("http://localhost:3000/poi")
                    .then(function(response) {
                        console.log(response.data.length);
                        for(row in response.data)
                        {
                        console.log(response.data[row])
                            $scope.dynMarkers.push({lat:response.data[row].lat, lng:response.data[row].lng});
                        }
                    });
            }

            vm.getPlaceInformation = function(marker,placeId) {
             var me = this;
            this.placesService = new google.maps.places.PlacesService(map);
             this.placesService.getDetails({placeId: placeId}, function(place, status) {
               if (status === 'OK') {
                 //me.infowindow.close();
                 marker.content =
                   '<div ><img src="' + place.icon + '" height="16" width="16"> '
                   + '<strong>' + place.name + '</strong><br>' + 'Place ID: '
                   + place.place_id + '<br>' + place.formatted_address + '<br>'
                   +'</div>';
                 marker.title = place.name
               }
             });
           };

           vm.CheckPOI= function ( event ) {
                if (event.placeId) {
                    $scope.poiClicked =true;
                    $scope.lat = event.latLng.lat();
                    $scope.lng = event.latLng.lng();
                    $scope.placeId = event.placeId;
                }
                else
                {
                    $scope.poiClicked =false;
                    $scope.poiMarkClicked = false;
                }
           }




            vm.addMarker = function ( event ) {
//              var latLng = new google.maps.LatLng($scope.lat, $scope.lng);
//              var marker = new google.maps.Marker({position:latLng});
//
//              //this.calculateAndDisplayRoute(event.placeId);
//              vm.getPlaceInformation(marker,$scope.placeId);
//              marker.setMap(map)
//                $http.get("http://localhost:3000/poi")
//                    .then(function(response) {
//                        console.log(response.data);
//                    });
                var markerFind = vm.findMarker($scope.lat,$scope.lng);
                if(markerFind<=-1)
                {
                    $http({
                        method : 'POST',
                        url : 'http://localhost:3000/poi/create',
                        data : 'lat=' + $scope.lat + '&lng=' + $scope.lng,
                        headers : {
                            'Content-Type' : 'application/x-www-form-urlencoded'
                        }
                    })
                  $scope.dynMarkers.push({lat:$scope.lat, lng:$scope.lng});
              }
            }


            vm.delMarker = function ( event ) {
//              var latLng = new google.maps.LatLng($scope.lat, $scope.lng);
//              var marker = new google.maps.Marker({position:latLng});
              var index = vm.findMarker($scope.lat, $scope.lng);
//              console.log(index)
//              console.log($scope.dynMarkers.length)
              if(index>-1)
              {
                    $scope.dynMarkers.splice(index, 1);
                  for (var key in map.markers) {
                      console.log(map.markers[key].position.lat());
                      if(map.markers[key].position.lng() == $scope.lng && map.markers[key].position.lat() == $scope.lat)
                      {
                            map.markers[key].setMap(null);
                      }
                    };
                    $http({
                        method : 'DELETE',
                        url : 'http://localhost:3000/poi/delete',
                        data : 'lat=' + $scope.lat + '&lng=' + $scope.lng,
                        headers : {
                            'Content-Type' : 'application/x-www-form-urlencoded'
                        }
                    })
                    $scope.poiMarkClicked = false
                }
//              if(index>-1)
//              {
//                var marker = $scope.dynMarkers.splice(index, 1);
//                marker[0].setMap(null);
//                }
              //this.calculateAndDisplayRoute(event.placeId);
            }

            var directionsService = new google.maps.DirectionsService,
                directionsDisplay = new google.maps.DirectionsRenderer

            vm.dirMarker = function ( event ) {
//              var latLng = new google.maps.LatLng($scope.lat, $scope.lng);
//              var marker = new google.maps.Marker({position:latLng});
//              var index = vm.findMarker($scope.lat, $scope.lng);
//                var directionsDisplay = new google.maps.DirectionsRenderer();
//                var directionsService = new google.maps.DirectionsService();
                directionsDisplay.setMap(null);
                var origin =  new google.maps.LatLng($scope.dynMarkers[0].lat, $scope.dynMarkers[0].lng);
                var destination =  new google.maps.LatLng($scope.dynMarkers[$scope.dynMarkers.length-1].lat
                , $scope.dynMarkers[$scope.dynMarkers.length-1].lng);
                var waypoints = [];
                for(var i=0;i<$scope.dynMarkers.length;i++)
                {
                    if(i>0 && i<$scope.dynMarkers.length-1)
                    {
                        waypoints.push({
                            location: new google.maps.LatLng($scope.dynMarkers[i].lat, $scope.dynMarkers[i].lng),
                            stopover: true
                        });
                    }
                }
//                waypoints.push({
//                    location: new google.maps.LatLng($scope.dynMarkers[1].lat, $scope.dynMarkers[1].lng),
//                    stopover: true
//                });
                var request = {
                  origin: origin,
                  destination: destination,
                  waypoints: waypoints, //an array of waypoints
                    optimizeWaypoints: true,
                  travelMode: google.maps.DirectionsTravelMode.DRIVING
                };
                directionsService.route(request, function (response, status) {
                  if (status === google.maps.DirectionsStatus.OK) {
                    directionsDisplay.setDirections(response);
                    directionsDisplay.setMap(map);
//                    directionsDisplay.setPanel(document.getElementById('directionsList'));
//                    $scope.directions.showList = true;
                  } else {
                    alert('Google route unsuccesfull!');
                  }
                });
//                for (var key in map.markers) {
//                  console.log(map.markers[key].position.lat());
//                  if(map.markers[key].position.lng() == $scope.lng && map.markers[key].position.lat() == $scope.lat)
//                  {
//                        map.markers[key].setMap(null);
//                  }
//                };
//              if(index>-1)
//              {
//                var marker = $scope.dynMarkers.splice(index, 1);
//                marker[0].setMap(null);
//                }
              //this.calculateAndDisplayRoute(event.placeId);
            }

            vm.findMarker = function(lat,lng){
                for(var i=0;i<$scope.dynMarkers.length;i++)
                {
//                    console.log($scope.dynMarkers[i])
//                    console.log("lat:"+lat+"lng:"+lng)
                    if($scope.dynMarkers[i].lat==lat && $scope.dynMarkers[i].lng==lng)
                        return i
                }
                return -1;
//            console.log("lat"+lat);
//                for(var i=0;i<map.markers.length;i++)
//                {
//                console.log(map.markers[i]);
////                    if($scope.dynMarkers[i].position.lng == lng && $scope.dynMarkers[i].position.lat == lat)
////                        return i;
//                }
//                return -1
            }
         });