

angular.module('main.controllers', ['ngMap']).
controller('mapController', function($scope,NgMap, $http,mapAPIservice) {
    $scope.poiClicked = false;
    $scope.poiMarkClicked = false;
    $scope.poiMarkClick = "asdasdas"
    $scope.lat = 0;
    $scope.lng = 0;
    $scope.placeId = "";
    $scope.notifUser = "Click on any Point of Interest or marker to start using this page";
    var map;
    var vm = this;
    $scope.dynMarkers = [];
    var directionsService = new google.maps.DirectionsService,
        directionsDisplay = new google.maps.DirectionsRenderer
    $scope.googleMapsUrl = "https://maps.googleapis.com/maps/api/js?key=AIzaSyBCwWBHFM9kBg3y5KTKuVRTJlTmIR_sGSc&libraries=places";
    NgMap.getMap().then(function(evtMap) {
         console.log(evtMap.getCenter());
         console.log('markers', evtMap.markers);
         console.log('shapes', evtMap.shapes);
         map = evtMap;
            vm.populateMarker()
    });

    /**
     * get marker position and notify variable that there is a marker clicked
     * @param {object} events - click event.
     * @param {object} marker - marker object.
     */
    vm.markerClick = function(events, marker) {
        var pos = marker.$index;
        $scope.lat = $scope.dynMarkers[pos].lat;
        $scope.lng = $scope.dynMarkers[pos].lng;
        $scope.poiMarkClicked = true;
        $scope.notifUser = "you clicked on POI marker click delete poi to delete a marker";
    }

    /**
     * populate marker on map from database
     * @function
     */
    vm.populateMarker = function() {
        try {
            console.log(mapAPIservice.getAll());
            mapAPIservice.getAll().success(function (response) {
                //Digging into the response to get the relevant data
                for(row in response) {
                    $scope.dynMarkers.push({lat:response[row].lat,
                        lng:response[row].lng});
                }
            }).fail(function (response){console.log(response)});
        }
        catch(e){
            console.log(e);
        }
    }

    /**
     * check clicked event if this is a POI, if POI then notify that this is a POI
     * @function
     * @param {object} events - click event.
     */
    vm.CheckPOI = function ( event ) {
        if (event.placeId) {
            $scope.poiClicked = true;
            $scope.lat = event.latLng.lat();
            $scope.lng = event.latLng.lng();
            $scope.placeId = event.placeId;
            var markerFind = vm.findMarker($scope.lat,$scope.lng);
            if(markerFind <= -1) {
                $scope.notifUser = "you clicked on point of interest click add poi to create a marker";
            }

        } else {
            $scope.poiClicked = false;
            $scope.poiMarkClicked = false;
            $scope.notifUser = "Click on any Point of Interest or marker to start using this page";
        }
   }

    /**
     * create marker from POI that has been clicked
     * @function
     * @param {object} events - click button event.
     */
    vm.addMarker = function ( event ) {
        var markerFind = vm.findMarker($scope.lat,$scope.lng);

        if(markerFind <= -1) {
            mapAPIservice.addPOI($scope.lat,$scope.lng).success(
                function (response) {
            });
            $scope.dynMarkers.push({lat:$scope.lat, lng:$scope.lng});
            if( $scope.dynMarkers.length > 1){
                $scope.notifUser = "you have more than 1 marker, you can start creating shortest route with clicking direction button ";
            } else {
                $scope.notifUser = "you must create 1 more marker to start getting a direction";
            }
        }
    }

    /**
     * delete marker from marker that has been clicked previously
     * @function
     * @param {object} events - click button event.
     */
    vm.delMarker = function ( event ) {
        var index = vm.findMarker($scope.lat, $scope.lng);
            if(index >- 1) {
                $scope.dynMarkers.splice(index, 1);
                for (var key in map.markers) {
                    console.log(map.markers[key].position.lat());
                    if(map.markers[key].position.lng() == $scope.lng
                            && map.markers[key].position.lat() == $scope.lat) {
                        map.markers[key].setMap(null);
                    }
                };
                mapAPIservice.delPOI($scope.lat,$scope.lng).success(function (response) {
                });
                $scope.poiMarkClicked = false
            }
    }

    /**
     * stringify lat and long marker to create static map param
     * @function
     * @param {string} string - marker param string.
     * @param {object} markerPoint - tuple or object consist of lat and lng.
     * @returns {string} append result of string param and markrpoint coordinate
     */
    vm.staticStringifyMarker = function(str,markerPoint) {
        return str + "|" + markerPoint.lat + "," + markerPoint.lng;
    }

    /**
     * export map to static map jpg
     * @function
     * @param {object} event- click button event.
     */
    vm.exportMarker = function(event) {
        var origin =  new google.maps.LatLng($scope.dynMarkers[0].lat,
            $scope.dynMarkers[0].lng);
        var destination =  new google.maps.LatLng
            ($scope.dynMarkers[$scope.dynMarkers.length - 1].lat
                , $scope.dynMarkers[$scope.dynMarkers.length - 1].lng);
        var wayPoints = [];
        var staticMarker = "&markers=color:red|" + $scope.dynMarkers[0].lat
            + "," + $scope.dynMarkers[0].lng;
        for(var i = 0;i < $scope.dynMarkers.length;i++) {
            if(i > 0 && i < $scope.dynMarkers.length - 1) {
                staticMarker = vm.staticStringifyMarker
                    (staticMarker,$scope.dynMarkers[i]);
                wayPoints.push( {
                    location: new google.maps.LatLng(
                        $scope.dynMarkers[i].lat, $scope.dynMarkers[i].lng),
                    stopover: true
                });
            }
        }
        staticMarker += "|" +
            $scope.dynMarkers[$scope.dynMarkers.length - 1].lat + "," +
            $scope.dynMarkers[$scope.dynMarkers.length - 1].lng;
        var request = {
            origin: origin,
            destination: destination,
            waypoints: wayPoints, //an array of waypoints
            optimizeWaypoints: true,
            travelMode: google.maps.DirectionsTravelMode.DRIVING
        };
        directionsService.route(request, function (response, status) {
            console.log(response);
            if (status === google.maps.DirectionsStatus.OK) {
                window.location =
                    "http://maps.googleapis.com/maps/api/staticmap?size=400x400&path=enc:"
                        + response.routes[0].overview_polyline+staticMarker;
//                directionsDisplay.setDirections(response);
//                directionsDisplay.setMap(map);
            } else {
                alert('Google route unsuccesfull!');
            }
        });
    }

    /**
     * get direction from google api and display them on map
     * @function
     * @param {object} event - click button event.
     */
    vm.dirMarker = function ( event ) {
        directionsDisplay.setMap(null);
        var origin =  new google.maps.LatLng($scope.dynMarkers[0].lat,
            $scope.dynMarkers[0].lng);
        var destination =  new google.maps.LatLng
            ($scope.dynMarkers[$scope.dynMarkers.length - 1].lat,
                $scope.dynMarkers[$scope.dynMarkers.length - 1].lng);
        var wayPoints = [];
        for(var i = 0;i < $scope.dynMarkers.length;i++) {
            if(i > 0 && i < $scope.dynMarkers.length - 1) {
                wayPoints.push( {
                    location: new google.maps.LatLng($scope.dynMarkers[i].lat,
                        $scope.dynMarkers[i].lng),
                    stopover: true
                });
            }
        }
        var request = {
            origin: origin,
            destination: destination,
            waypoints: wayPoints, //an array of waypoints
            optimizeWaypoints: true,
            travelMode: google.maps.DirectionsTravelMode.DRIVING
        };
        directionsService.route(request, function (response, status) {
            console.log(response);
            if (status === google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(response);
                directionsDisplay.setMap(map);
                $scope.notifUser = "you can export this route by clicking export button";
            } else {
                alert('Google route unsuccesfull!');
            }
        });
    }

    /**
     * find marker index from given param
     * @function
     * @param {string} lat - latitude.
     * @param {string} lng - longitude.
     * @returns {int} index marker, if not return -1
     */
    vm.findMarker = function(lat,lng) {
        for(var i = 0;i < $scope.dynMarkers.length;i++) {
            if($scope.dynMarkers[i].lat == lat && $scope.dynMarkers[i].lng == lng) {
                return i
            }
        }
        return -1;
    }


});