/**
 * @author Example <jon.doe@example.com>
 * @copyright 2014 Example Ltd. All rights reserved.
 */

 /**
     * An Example Angular Bootstrap Module
     * @memberOf example.angular
     * @namespace example.angular.bootstrap
     * @function bootstrap
     * @example
     *     &lt;div ng-app="exampleAngularBootstrap"&gt;
     *         &lt;div ng-view&gt;&lt;/div&gt;
     *     &lt;/div&gt;
     */
angular.module('main.services', []).
    factory('mapAPIservice', function($http) {

    var mapAPI = {};

    /**
     * get all POI from database
     * @function
     * @returns {array} result ajax
     */
    mapAPI.getAll = function() {
        return $http({
            method: 'GET',
            url: backendurl
        });
    }

    /**
     * add POI coordinate to database
     * @function
     * @param {string} lat - latitude.
     * @param {string} lng - longitude.
     * @returns {string} result ajax
     */
    mapAPI.addPOI = function(lat,lng) {
        return $http( {
            method : 'POST',
            url : backendurl+'/create',
            data : 'lat=' + lat + '&lng=' + lng,
            headers : {
                'Content-Type' : 'application/x-www-form-urlencoded'
            }
        })
    }

    /**
     * delete POI coordinate to database
     * @function
     * @param {string} lat - latitude.
     * @param {string} lng - longitude.
     * @returns {string} result ajax
     */
    mapAPI.delPOI = function(lat,lng) {
        return $http( {
            method : 'DELETE',
            url : backendurl+'/delete',
            data : 'lat=' + lat + '&lng=' + lng,
            headers : {
                'Content-Type' : 'application/x-www-form-urlencoded'
            }
        })
    }


    return mapAPI;
  });