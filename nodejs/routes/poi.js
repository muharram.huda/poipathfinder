/**
 * poi controller module.
 * @module routes/poi
 */

var models  = require('../models');
var express = require('express');
var router = express.Router();

/**
 * get all POI coordinates
 * @function
 * @returns {array} POI coordinates
 */
router.get('/', function(req, res) {
  models.tblpoi.getPOI().then(function (tes) {
        res.json(tes);
    });

});

/**
 * create a POI coordinate
 * @function
 * @param {string} req.body.lat - latitude.
 * @param {string} req.body.lng - longitude.
 * @returns {string} add process status result
 */
router.post('/create', function(req, res) {
    console.log(req.body)
    models.tblpoi.addPOI( req.body.lat,req.body.lng).then(function() {
        res.send("add success");
    }).catch(function(error){
        res.send("add error");
    });;
});

/**
 * delete a POI coordinate
 * @function
 * @param {string} req.body.lat - latitude.
 * @param {string} req.body.lng - longitude.
 * @returns {string} delete process status result
 */
router.delete('/delete', function(req, res) {
    console.log(req.body);
//    console.log(models.tblpoi);
    models.tblpoi.deletePOI(req.body.lat,req.body.lng).then(function() {
        res.send("delete success");
    }).catch(function(error) {
        res.send("delete error");
    });

});

module.exports = router;
